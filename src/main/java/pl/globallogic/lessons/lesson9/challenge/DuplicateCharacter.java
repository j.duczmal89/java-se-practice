package pl.globallogic.lessons.lesson9.challenge;

import java.util.HashMap;
import java.util.Map;

public class DuplicateCharacter {
    public static void main(String[] args) {
        String source = "Poland";
        System.out.printf("Does '%s' have duplicate character: '%s'", source, isContainingDuplicateChars(source));
    }

    private static boolean isContainingDuplicateChars(String source) {
        Map<Character, Integer> resultMap = new HashMap<>();
        String lowerCaseSource = source.toLowerCase();
        for (int i=0; i<source.length(); i++){
            char key = source.charAt(i);
            if (!resultMap.containsKey(key)){
                resultMap.put(key, 1);
            } else {
                int count = resultMap.get(key);
                resultMap.put(key, ++count);
            }
        }
        for (Map.Entry<Character,Integer> entry: resultMap.entrySet()){
            if (entry.getValue()>1)return true;
        } return false;
    }
}
