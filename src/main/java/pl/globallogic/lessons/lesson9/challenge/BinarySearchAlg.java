package pl.globallogic.lessons.lesson9.challenge;

public class BinarySearchAlg {
    public static void main(String[] args) {
        int[] source = {2,5,7,9,10,34,56};
        int element = 10;
        int actualPosition = 4;
        System.out.println("Do we have our element in array:   " + (searchFor(source, element) == actualPosition));
    }

    private static int searchFor(int[] source, int element) {
        int left =0, right = source.length-1;
        while(left<=right){
            int middle = left + (right-left)/2;
            if (source[middle]==element){
                return middle;
            }
            if (source[middle]<element){
                left = middle +1;
            }
            if (source[middle]>element){
                right = middle -1;
            }
        } return -1;
    }
}
