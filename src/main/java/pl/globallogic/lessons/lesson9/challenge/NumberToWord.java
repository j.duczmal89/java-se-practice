package pl.globallogic.lessons.lesson9.challenge;

import java.util.HashMap;
import java.util.Map;

public class NumberToWord {
    public static void main(String[] args) {
        int number = 1348;
        System.out.printf(" '%s' as wrods: '%s'", number, toWords(number));
    }

    private static String toWords(int number) {
        Map<Character, String> numberWordMapping = new HashMap<>();
        numberWordMapping.put('0', "Zero");
        numberWordMapping.put('1', "One");
        numberWordMapping.put('2', "Two");
        numberWordMapping.put('3', "Three");
        numberWordMapping.put('4', "Four");
        numberWordMapping.put('5', "Five");
        numberWordMapping.put('6', "Six");
        numberWordMapping.put('7', "Seven");
        numberWordMapping.put('8', "Eight");
        numberWordMapping.put('9', "Nine");
        StringBuilder result = new StringBuilder();
        String numberAsString = String.valueOf(number);
        for (int i=0; i < numberAsString.length(); i++){
            String token = numberWordMapping.get(numberAsString.charAt(i));
            result.append(token).append(" ");
        }return result.toString().trim();

    }
}
