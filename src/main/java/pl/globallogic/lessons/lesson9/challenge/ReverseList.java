package pl.globallogic.lessons.lesson9.challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseList {
    public static void main(String[] args) {
        List<Integer> input = List.of(2,3,4,8,9,5);
        System.out.println("Reversed input list: "+reverse(input));
        Collections.reverse(input);
        System.out.println("Reversed input list (JDK): "+input);
    }

    private static ArrayList<Integer> reverse(List<Integer> input){
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = input.size() -1; i > 0; i--){
            result.add(input.get(i));
        }
        return result;
    }
}
