package pl.globallogic.lessons.lesson9.challenge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class RemoveDuplicatesSet {
    public static void main(String[] args) {
        List<String> usernames = List.of("user1", "user2","user1","user3","test_user","admin_user");
        System.out.println("# of uniqe usernames in the system -> " + countNumberOfUniqeNames(usernames));

    }

    private static int countNumberOfUniqeNames(List<String> usernames) {
        return new ArrayList<>(new HashSet<>(usernames)).size();
    }
}
