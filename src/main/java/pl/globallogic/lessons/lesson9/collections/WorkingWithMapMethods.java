package pl.globallogic.lessons.lesson9.collections;

import pl.globallogic.lessons.lesson9.collections.Fruit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WorkingWithMapMethods {
    public static void main(String[] args) {
        Map<String, Fruit> fruitMap = new HashMap<>();
        Fruit apple = new Fruit("Apple","Poland",15);
        Fruit bananas= new Fruit("Bananas","Equador",25);
        Fruit orange= new Fruit("Orange","Spain",18);
        Fruit orange2= new Fruit("Orange 2","Spain",18);
        //Fruit orange3= new Fruit("Orange replaced","Spain",28);
        Fruit orange3 = null;
        fruitMap.put("Apple", apple);
        fruitMap.put("Bananas", bananas);
        fruitMap.put("Orange", orange);
        fruitMap.put("Orange", orange2);
        for (String key: fruitMap.keySet()){
            System.out.println("Key: " + key);
        }
        for (Fruit fruit: fruitMap.values()){
            System.out.println("Fruit: " + fruit);
        }
        System.out.println(fruitMap.get("Apple"));
        System.out.println("# of fruits:" + fruitMap.size());

        for (Map.Entry<String, Fruit> fruitPair : fruitMap.entrySet()){
            System.out.printf("Fruit '%s' have a label '%s' \n", fruitPair.getValue(), fruitPair.getKey());
        }

        for (Map.Entry<String, Fruit> fruitPair : fruitMap.entrySet()){
            if (fruitPair.getKey().equalsIgnoreCase("Orange"))
                fruitPair.setValue(orange3);
        }
        System.out.println("Orange key fruit: " +fruitMap.get("Orange"));



    }
}


