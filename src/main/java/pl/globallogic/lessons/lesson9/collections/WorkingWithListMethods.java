package pl.globallogic.lessons.lesson9.collections;

import java.util.*;

import pl.globallogic.lessons.lesson9.collections.Fruit;

public class WorkingWithListMethods {
    public static void main(String[] args) {
        ArrayList<String> fruitBucket = new ArrayList<>(20);
        List<Integer> numbers = List.of(1,2,3,4,5,6); // <- imutable list creatin - no add/remove
        //numbers.add(23);
        //numbers.remove(3);

        fruitBucket.add("Apple");
        fruitBucket.add("Orange");
        fruitBucket.add("Lemon");
        fruitBucket.add("banana");
        fruitBucket.add("apple");
        fruitBucket.add("orange");
        fruitBucket.add("1lemon");
        fruitBucket.add("2banana");
        System.out.printf("# of items in our fruit bucket: '%s' \n", fruitBucket.size());
        System.out.printf("Is our fruit bucket: '%s' \n", fruitBucket.isEmpty());

        fruitBucket.add(2,"Grape");
        System.out.println(fruitBucket);
        List<String> smallBucket = List.of("Strawberry", "Blueberry");
        fruitBucket.addAll(smallBucket);
        System.out.println(fruitBucket);

        fruitBucket.set(3, "Dragon Fruit");
        System.out.println(fruitBucket);
        int position = 3;
        System.out.printf("On postion '%s' in our bucket we have '%s'\n",position, fruitBucket.get(position-1));
        System.out.println( fruitBucket.removeAll(List.of("bananas","Blueberry")));
        System.out.println(fruitBucket);
        String fruitToSeek = "Grape";
        System.out.printf("%s is in %s position \n",fruitToSeek,fruitBucket.indexOf(fruitToSeek)+1);
        String[] oldBucket = {"Apple","Peach","Bananas"};
        List<String> newFruitBucket = Arrays.asList(oldBucket);
        newFruitBucket.set(1,"Blackberry");
        System.out.println(newFruitBucket);
        System.out.println("Our old bucket: " + Arrays.asList(oldBucket));
        System.out.println(Arrays.toString(fruitBucket.toArray()));
        System.out.println("***************End of 1st iteration of List overview ****************************");

        Fruit apple = new Fruit("Apple","Poland",15);
        Fruit bananas= new Fruit("Bananas","Equador",25);
        Fruit orange= new Fruit("Orange","Spain",18);

        Fruit appleToSeek = new Fruit("Apple","Poland",15);

        ArrayList<Fruit> enhancedFruitBucket = new ArrayList<>();
        enhancedFruitBucket.add(apple);
        enhancedFruitBucket.add(orange);
        enhancedFruitBucket.add(bananas);
        System.out.println("Index of apple in a list -> "+enhancedFruitBucket.indexOf(appleToSeek));
        System.out.println("Index of apple in a list -> "+enhancedFruitBucket.indexOf(apple));
        System.out.println("*********************************************************");
        System.out.println(fruitBucket);
        //fruitBucket.sort(Comparator.naturalOrder());
        Collections.sort(fruitBucket);
        System.out.println(fruitBucket);
        System.out.println("*********************************************************");
        ListIterator<Fruit> fruitsIterator = enhancedFruitBucket.listIterator();
        while (fruitsIterator.hasNext()){
            System.out.println(fruitsIterator.next());
        }


    }
}
