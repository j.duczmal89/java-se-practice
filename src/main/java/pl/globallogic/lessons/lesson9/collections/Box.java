package pl.globallogic.lessons.lesson9.collections;

public class Box <T> {

    private T payload;

    public Box(T payload) {
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
