package pl.globallogic.lessons.lesson9.collections;

import java.util.ArrayList;
import java.util.List;

public class NongenericCollectionSandbox {
    public static void main(String[] args) {


        List nonGenericNameStorage = new ArrayList<>();
        nonGenericNameStorage.add("Joe");
        nonGenericNameStorage.add("Jane");
        nonGenericNameStorage.add("Jack");
        nonGenericNameStorage.add(3);
        nonGenericNameStorage.add(new StringBuilder("Cos tam"));
        for (Object name : nonGenericNameStorage) {
            if (name instanceof String)
                System.out.println(((String) name).toUpperCase());
        }

        /*List<String> wtedy jest generic
        List<String> genericNameStorage = new ArrayList<>();
        genericNameStorage.add("Joe");
        genericNameStorage.add("Jane");
        genericNameStorage.add("Jack");
        for (String name : genericNameStorage) {
            System.out.println(name.toUpperCase());
        }*/
    }
}
