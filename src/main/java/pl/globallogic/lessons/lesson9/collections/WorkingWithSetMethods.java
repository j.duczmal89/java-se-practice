package pl.globallogic.lessons.lesson9.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class WorkingWithSetMethods {
    public static void main(String[] args) {
        Fruit apple = new Fruit("Apple","Poland",15);
        Fruit bananas= new Fruit("Bananas","Equador",25);
        Fruit orange= new Fruit("Orange","Spain",18);
        Fruit sameorange= new Fruit("Orange","Spain",18);

        Set<Fruit> bucketWithUniqeFruits = new HashSet<>();
        bucketWithUniqeFruits.add(apple);
        bucketWithUniqeFruits.add(bananas);
        bucketWithUniqeFruits.add(orange);
        bucketWithUniqeFruits.add(sameorange);
        for (Fruit fruit: bucketWithUniqeFruits){
            System.out.println(fruit);
        }
        System.out.println("Do we have orange? #" + bucketWithUniqeFruits.contains(orange));
        TreeSet<Fruit> sortedFruitsBucket = new TreeSet<>();
        sortedFruitsBucket.add(apple);
        sortedFruitsBucket.add(bananas);
        sortedFruitsBucket.add(orange);
        for (Fruit fruit : sortedFruitsBucket){
            System.out.println(fruit);
        }
    }

}
