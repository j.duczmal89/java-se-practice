package pl.globallogic.lessons.lesson9.collections;

import java.util.Comparator;
import java.util.Objects;

public class Fruit implements Comparable<Fruit> {
    private String title;
    private String countryOfOrigin;
    private int weight;

    public Fruit(String title, String countryOfOrigin, int weight) {
        this.title = title;
        this.countryOfOrigin = countryOfOrigin;
        this.weight = weight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    /*@Override
    public int compare(Fruit o1, Fruit o2) {
        if (o1.getCountryOfOrigin().length() > o2.getCountryOfOrigin().length()) {
            return 1;
        } else if (o1.getCountryOfOrigin().length() < o2.getCountryOfOrigin().length()) {
            return -1;
        } else {
            return 0;
        }*/


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fruit fruit = (Fruit) o;
        return weight == fruit.weight && Objects.equals(title, fruit.title) && Objects.equals(countryOfOrigin, fruit.countryOfOrigin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, countryOfOrigin, weight);
    }

    @Override
    public int compareTo(Fruit o) {
        if (this.countryOfOrigin.length()>o.countryOfOrigin.length()) return -1;
        if (this.countryOfOrigin.length()<o.countryOfOrigin.length()) return 1;
        return 0;

    }
    @Override
    public String toString() {
        return "Fruit(title: " + title + ", countryOfOrigin: " + countryOfOrigin + ", weight: " + weight + ")";
    }

}
