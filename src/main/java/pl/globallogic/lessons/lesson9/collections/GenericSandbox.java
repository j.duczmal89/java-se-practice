package pl.globallogic.lessons.lesson9.collections;

import pl.globallogic.lessons.lesson9.collections.Box;

public class GenericSandbox {
    public static void main(String[] args) {
        Box<String> smallBox = new Box<>("Jane");
        smallBox.setPayload("Jane");
        String name=smallBox.getPayload();

    }
}
