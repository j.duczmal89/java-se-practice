package pl.globallogic.lessons.lesson9.interfaces;

public class Car implements Drivable{

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void ignite() {

    }
}
