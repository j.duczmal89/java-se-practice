package pl.globallogic.lessons.lesson9.abstractclasses;

public abstract class Vehicle {
    protected int numberOfWheels;
    protected String producer;
    protected int numberOfGears;

    public Vehicle(int numberOfWheels, String producer) {
        this.numberOfWheels = numberOfWheels;
        this.producer = producer;
    }

    abstract public void drive();

    public void switchLights(){
        System.out.println("Switch lights in vehicle");
    }
}
