package pl.globallogic.lessons.lesson9.abstractclasses;

public class Car extends Vehicle{

    private String fuel;
    public Car(String producer, String fuel) {
        super(4, producer);
        this.fuel=fuel;
    }

    @Override
    public void drive() {
        ignite();
        pressClutch();
        selectGear(1);


    }
    @Override
    public void switchLights(){
        super.switchLights();
        System.out.println("Switching light in car");
    }

    private void selectGear(int gear) {
        System.out.printf("Switched to %s gear. \n", gear);
    }

    private void pressClutch() {
        System.out.println("Clutch have been pressed.");
    }

    private void ignite() {
        System.out.println("Ignition...");
    }
}
