package pl.globallogic.lessons.lesson6.Challenges;

public class CapitalizeText {
    public static void main(String[] args) {
        String targetText = "i love programming.";
        String expectedResult = "I Love Programming.";
        System.out.println(expectedResult.contentEquals(processAsCharArray(targetText)));
        System.out.println(expectedResult.contentEquals(processWithSplit(targetText)));
    }

    private static String processWithSplit(String targetText) {
        String[] tokens = targetText.split(" ");
        StringBuilder result = new StringBuilder("");
        for (String token: tokens){
            result.append(capitalize(token)).append(" ");
        }
        return result.toString().trim();
    }

    private static String capitalize(String token){
        if (token.isEmpty()) return "";
        String firstLetterCapitalized = token.substring(0,1).toUpperCase();
        String restOfTheToken = token.substring(1);
        return firstLetterCapitalized+restOfTheToken;
    }


    private static String processAsCharArray(String targetText) {
        char[] textAsChars = targetText.toCharArray();
        boolean capitalizeNextChar = true;
        for (int i=0; i <textAsChars.length; i++){
            char currentChar = textAsChars[i];
            if (Character.isWhitespace(currentChar)){
                capitalizeNextChar = true;
            } else {
                if (capitalizeNextChar && Character.isLetter(currentChar)){
                    textAsChars[i] = Character.toUpperCase(currentChar);
                    capitalizeNextChar = false;
                }
            }
        }return new String(textAsChars);
    }


}
