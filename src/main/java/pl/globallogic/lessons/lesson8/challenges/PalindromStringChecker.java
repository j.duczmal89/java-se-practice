package pl.globallogic.lessons.lesson8.challenges;

public class PalindromStringChecker {
    public static void main(String[] args) {
        System.out.println(isPalindrome("racecar"));  // Output: true
        System.out.println(isPalindrome("hello"));    // Output: false
        System.out.println(isPalindrome("level"));    // Output: true
        System.out.println(isPalindrome("world"));    // Output: false
    }

    public static boolean isPalindrome(String targetString) {
        targetString = targetString.toLowerCase(); // Convert to lowercase for case-insensitive comparison
        int left = 0;
        int right = targetString.length() - 1;

        while (left < right) {
            if (targetString.charAt(left) != targetString.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }

        return true;
    }
}
