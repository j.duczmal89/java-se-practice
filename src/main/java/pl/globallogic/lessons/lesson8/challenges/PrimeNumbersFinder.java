package pl.globallogic.lessons.lesson8.challenges;

import java.util.Scanner;

public class PrimeNumbersFinder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the starting number: ");
        int startingNumber = scanner.nextInt();

        System.out.print("Enter the ending number: ");
        int endingNumber = scanner.nextInt();

        System.out.println("Prime numbers between " + startingNumber + " and " + endingNumber + ":");
        findPrimesInRange(startingNumber, endingNumber);

        scanner.close();
    }

    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    public static void findPrimesInRange(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (isPrime(i)) {
                System.out.print(i + " ");
            }
        }
    }
}

