package pl.globallogic.lessons.lesson5.challenge;

public class VowelConsonantCounter {


    public static void main(String[] args) {
        String word = "Arrays";
        int[] result = numberOfVowelIsAndConsonants(word);

        System.out.println(String.format("Number of vowels is %s and consonants is %s in '%s'",
                result[0],result[1],word));
    }

    private static int[] numberOfVowelIsAndConsonants(String word){
        int vowelsCount = 0;
        String vowels ="aeuioyAEYUIO";
        for (int i=0; i<word.length(); i++){
            if (vowels.contains(String.valueOf(word.charAt(i)))){
                vowelsCount++;
            }
        }
        return new int[] {vowelsCount,word.length()-vowelsCount};
    }
}
