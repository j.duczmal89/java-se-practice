package pl.globallogic.lessons.lesson5;
import pl.globallogic.lessons.lesson3.User;
import java.util.Arrays;

public class ArraySandbox {
    public static void main(String[] args) {


        String name1 = "Joe";
        String name2 = "Jane";
        String name3 = "Ivan";
        String[] names = {"Joe", "Jane", "Ivan"};
        User[] users = {new User("Joe"), new User("Jane")};


        User joe = users[2];
        //joe.name = "John";
        String joeName = names[0];
        //joeName = "John";

        System.out.println(users[0]);


        for (int i=0; i < users.length; i++) {
            System.out.println(users[i]);

        }
        for (User user: users) {
            System.out.println(user);
        }


        System.out.println(Arrays.toString(users));

        int[] unsortedNumbers = {6, 6, 4, 6, 8, 9, 3, 12, 10};
        Arrays.sort(unsortedNumbers);
        System.out.println(Arrays.toString(unsortedNumbers));

        System.out.println(Arrays.binarySearch(unsortedNumbers, 8)==4);

        int[] peaceOfOurSortedArray = Arrays.copyOfRange(unsortedNumbers, 1,4);
        int[] sortedCopyOfArray = Arrays.copyOf(unsortedNumbers,16);
        String[] newNameArray = Arrays.copyOf(names,16);
        System.out.println(Arrays.toString(peaceOfOurSortedArray));

        int[] arrayOf_100 = new int[20];
        Arrays.fill(arrayOf_100, 100);

        int[] numbers1 = {1,2,3,8,1};
        int[] numbers2 = {1,2,3,5,5};

        System.out.println(Arrays.compare(numbers1,numbers2));
        System.out.println(Arrays.equals(numbers1,numbers2));

    }
}
