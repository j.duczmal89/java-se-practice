package pl.globallogic.lessons.lesson4;

public class StringsSandbox {
    public static void main(String[] args) {
        String greeting = "Hello";
        String longText = """
                 to
                moze 
                byc 
                dlugie""";

        //better no to use one below
        //String greeting2 = "Czesc";

        String greeting3 = new String("Hello");
        System.out.println(greeting == greeting3);
        System.out.println(greeting.equals(greeting3));
        greeting=greeting.toUpperCase();
        System.out.println(greeting);

        String example = greeting3+ longText;
        System.out.println(example);
        //string length
        System.out.println("Length of our longText is "+ longText.length() );
        System.out.println("Length of example is "+ example.length());

        //extarct character
        for (int i=0; i<example.length(); i++){
            System.out.println(example.charAt(i));
        }
        String greetWithSpaces = "      elo       ";
        String blankText = "";
        //upper,lower case
        System.out.println(greeting3.toUpperCase());
        System.out.println(greeting3.toLowerCase());
        System.out.println(greetWithSpaces);
        System.out.println(greetWithSpaces.trim());

        //check string if is empty, blank
        System.out.println(blankText.isBlank());
        System.out.println(blankText.isEmpty());

        System.out.println(longText.substring(0,6));

        //check if if strings starts or ends with specific string
        System.out.println(greeting3.startsWith("H"));
        System.out.println(greeting3.endsWith("f"));
        System.out.println(greeting3.contains("el"));
        System.out.println(greeting3.equalsIgnoreCase("hello"));
        System.out.println(greeting3.contentEquals("Hello"));

        //replace
        System.out.println(greeting3.replace("Hel","e"));

        //format


    }
}
