package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class Ex5LeapYearCalculator {
    public static boolean isLeapYear(int year){
        if (year < 1 || year > 9999){
            return false;
        }
        return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);

    }
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year");
        int year = scanner.nextInt();
        if (isLeapYear(year)){
            System.out.println("Year "+ year + " the leap year");}
        else {
            System.out.println("Year "+ year + " is not the leap year");}

        scanner.close();

        //System.out.println(isLeapYear(-1600));
        //System.out.println(isLeapYear(1600));
        //System.out.println(isLeapYear(2017));
        //System.out.println(isLeapYear(2000));

    }
}
