package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class Ex1PosNegZer {
    public static void checkNumber(int number) {
        if (number > 0) {
            System.out.println("positive");
        } else if (number < 0) {
            System.out.println("negative");
        } else {
            System.out.println("zero");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int userInput = scanner.nextInt();

        checkNumber(userInput);
        scanner.close();
    }
}
