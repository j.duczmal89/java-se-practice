package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class PowerOfNumberCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number to rise");
        int base = scanner.nextInt();
        System.out.println("Enter target power");
        int power = scanner.nextInt();
        System.out.println("Number "+base+ " in power " +power+ " is equal to "+calculatePower(base,power)  );

    }

    private static long calculatePower(int base, int power) {
        int result = 1;
        if (power < 0){
            return 0;
        } else if (power==0) {
            return 1;
        }
        else{
            for (int i=1; i<=power;i++){
                result *= base;

            }
            return result;
        }
    }
}
