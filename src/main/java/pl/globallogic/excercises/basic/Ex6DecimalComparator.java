package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class Ex6DecimalComparator {
    private static boolean areEqualByThreeDecimalPlaces(double number1, double number2){

        int num1Int = (int) (number1 * 1000);
        int num2Int = (int) (number2*1000);

        return num1Int==num2Int;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number");
        double number1 = scanner.nextDouble();
        System.out.println("Enter second number");
        double number2 = scanner.nextDouble();

        if (areEqualByThreeDecimalPlaces(number1,number2)){
            System.out.println("Number: " + number1 + " and number: " + number2 + "are equal by three decimal places") ;
        }
        else
            System.out.println("Number: " + number1 + " and number: " + number2 + "are not equal by three decimal places") ;

    }
}
