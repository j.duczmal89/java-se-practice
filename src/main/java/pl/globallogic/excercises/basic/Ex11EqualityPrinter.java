package pl.globallogic.excercises.basic;


import java.util.Scanner;

public class Ex11EqualityPrinter {
    private static void printEqual(int param1, int param2, int param3){
        if (param1<0 || param2<0 || param3<0){
            System.out.println("Invalid Value");
        }
        else if (param1==param2 && param2==param3){
            System.out.println("All numbers are equal");
        }
        else if (param1 != param2 && param1 != param3 && param2 != param3){
            System.out.println("All number are different");
        }
        else {
            System.out.println("Neither all are equal or different");
        }
    }
    public static void main(String[] args) {
        /*printEqual(1, 1, 1); //should print text All numbers are equal
        printEqual(1, 1, 2); //should print text Neither all are equal or different
        printEqual(-1, -1, -1); //should print text Invalid Value
        printEqual(1, 2, 3); //should print text All numbers are different*/

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 1st parameter: ");
        int param1 = scanner.nextInt();
        System.out.println("Enter 2nd parameter: ");
        int param2 = scanner.nextInt();
        System.out.println("Enter 3rd parameter: ");
        int param3 = scanner.nextInt();
        printEqual(param1,param2,param3);
    }
}
