package pl.globallogic.excercises.basic;
import java.util.Random;

public class Ex3MegaBytesCoverter {

    private static void printMegaBytesAndKiloBytes(int kiloBytes){

        if (kiloBytes == 0){
            System.out.println("Invalid value");
        } else {
            int megaBytes = kiloBytes / 1024;
            int remainingKiloBytes = kiloBytes % 1024;
            System.out.println(kiloBytes + " KB = " + megaBytes + " MB and " + remainingKiloBytes + " KB");
        }
    }
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(-1024);
        printMegaBytesAndKiloBytes(5000);

        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            int randomKiloBytes = random.nextInt(2 * 1024 * 1024) + 1;
            printMegaBytesAndKiloBytes(randomKiloBytes);
        }

    }
}
