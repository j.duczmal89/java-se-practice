package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class Ex7EqualSumChecker {
    private static boolean hasEqualSum(int number1, int number2, int number3){
        return (number1+number2)==number3;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter three numbers: ");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        int number3 = scanner.nextInt();
        if (hasEqualSum(number1,number2,number3)){
            System.out.println("Sum of number: "+ number1 + "  number: " + number2 + " are equal to:" + number3);
        }
        else {
            System.out.println("Sum of number: "+ number1 + " and number: " + number2 + " 1are not equal to:" + number3);
        }



    }
}
