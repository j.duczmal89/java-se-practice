package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class DigitCounter {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number to count digits");
        int number = scanner.nextInt();
        System.out.println("Digit count for "+ number + " is equal to " + numberOfDigits(number));
        scanner.close();

    }

    private static int numberOfDigits(int inputNumber) {
        if (inputNumber < 0){
            return -1;
        }else{
            int counter =0;
            while (inputNumber >0){
                inputNumber = inputNumber/10;
                counter++;
            }
           return counter;
        }
    }
}
