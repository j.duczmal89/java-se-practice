package pl.globallogic.excercises.basic;

import java.util.Scanner;
public class ExcerciseA {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number ");
        int number = scanner.nextInt();
        System.out.println(sumOfDigits(number));
    }

    public static int sumOfDigits(int number) {
        int sumOfDigits = 0;
        int temp = Math.abs(number);

        if (temp == 0) {
            return 0;
        }

        while (temp > 0) {
            sumOfDigits += temp % 10;
            temp /= 10;
        }
        return sumOfDigits;
    }
}



