package pl.globallogic.excercises.basic;


public class Ex12PlayingCat {
    private static boolean isCatPlaying(boolean summer,int temperature ){
        int tempLimit;
        if (summer) {
            tempLimit = 45;
        } else {
            tempLimit = 35;
        }
        return temperature >= 25 && temperature <= tempLimit;
    }
    public static void main(String[] args) {
        System.out.println(isCatPlaying(true, 10)); // false
        System.out.println(isCatPlaying(false, 36)); // false
        System.out.println(isCatPlaying(false, 35)); // true

    }
}
