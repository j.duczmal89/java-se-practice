package pl.globallogic.excercises.basic;

/*Write a method printYearsAndDays with parameter of type long named minutes.
        The method should not return anything (void) and it needs to calculate the years and days
        from the minutes parameter.
        If the parameter is less than 0, print text "Invalid Value".
        Otherwise, if the parameter is valid then it needs to print a message in the format "XX min = YY
        y and ZZ d".
        XX represents the original value minutes.
        YY represents the calculated years.
        ZZ represents the calculated days.
        EXAMPLES OF INPUT/OUTPUT:
        ● printYearsAndDays(525600); → should print "525600 min = 1 y and 0 d"
        ● printYearsAndDays(1051200); → should print "1051200 min = 2 y and 0 d"
        ● printYearsAndDays(561600); → should print "561600 min = 1 y and 25 d"*/

import java.util.Scanner;

public class Ex10MinutesToYearsAndDaysCalculator {
    private static void printYearsAndDays(long minutes){
        if (minutes < 0){
            System.out.println("Invalid Value");
        }
        long hours = minutes / 60;
        long days = hours / 24;
        long years = days / 365;
        days %= 365;

        System.out.println(minutes + " min = " + years + " year(s) and " + days + " day(s)");
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter minutes: ");
        long minutes = scanner.nextLong();
        printYearsAndDays(minutes);

    }
}
