package pl.globallogic.excercises.basic;

import java.util.Scanner;

public class Ex8TeenNumberChecker {
    private static boolean hasTeen(int age1, int age2, int age3){
        return isTeen(age1) || isTeen(age2) || isTeen(age3);
    }
    public static boolean isTeen(int num) {
        return num >= 13 && num <= 19;
    }
    public static void main(String[] args) {
        /*System.out.println(isTeen(9));
        System.out.println(isTeen(13));
        System.out.println(hasTeen(9, 99, 19));
        System.out.println(hasTeen(23, 15, 42));
        System.out.println(hasTeen(22, 23, 34));*/

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 1st person age: ");
        int age1 = scanner.nextInt();
        System.out.println("Enter 2nd person age: ");
        int age2 = scanner.nextInt();
        System.out.println("Enter 3rd person age: ");
        int age3 = scanner.nextInt();

        if (hasTeen(age1,age2,age3)){
            System.out.println("Someone from age: " + age1 + ", "+age2+", "+age3+" is a teen.");
        }
        else {
            System.out.println("None of them are teen");
        }



    }
}
