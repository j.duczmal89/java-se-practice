package pl.globallogic.excercises.basic;

public class Ex2SpeedConverter {

    public static void main(String[] args) {
        System.out.println(toMilesPerHour(1.5)==1);
        System.out.println(toMilesPerHour(10.25)==6);
        System.out.println(toMilesPerHour(-5.6)==-1);
        System.out.println(convertedSpeedMessage(1.5));
        System.out.println(convertedSpeedMessage(1.5).equals("1.5km/h =1mi/h"));
        System.out.println(convertedSpeedMessage(10.25).equals("10.25km/h =6mi/h"));
        System.out.println(convertedSpeedMessage(-5.6).equals("Invalid value"));

    }

    private static String convertedSpeedMessage(double kilometersPerHour) {
        long milesPerHour = toMilesPerHour(kilometersPerHour);
        if (milesPerHour == -1){
            return "Invalid value";
        } else {
            return ""+kilometersPerHour + "km/h =" + milesPerHour + "mi/h";
        }
    }
    private static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour < 0) {
            return -1;
        }
        return Math.round(kilometersPerHour / 1.609);
    }
}
