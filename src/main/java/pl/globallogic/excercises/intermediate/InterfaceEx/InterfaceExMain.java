package pl.globallogic.excercises.intermediate.InterfaceEx;

import java.util.List;

public class InterfaceExMain {
    public static void main(String[] args) {
        Player player = new Player("Tim", 10, 15);
        System.out.println(player.toString());

        List<String> playerValues = player.write();
        Player newPlayer = new Player("", 0, 0);
        newPlayer.read(playerValues);
        System.out.println(newPlayer.toString());

        Monster monster = new Monster("Werewolf", 20, 40);
        System.out.println(monster.toString());

        List<String> monsterValues = monster.write();
        Monster newMonster = new Monster("", 0, 0);
        newMonster.read(monsterValues);
        System.out.println(newMonster.toString());
    }
}
