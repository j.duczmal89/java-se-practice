package pl.globallogic.excercises.intermediate.InterfaceEx;

import java.util.List;

public interface IsSaveable {
    List<String> write();
    void read(List<String> savedValues);
}
