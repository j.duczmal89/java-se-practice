package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class WallArea {
    public static class Wall {
        private double width;
        private double height;

        public Wall() {
            this.width = 0;
            this.height = 0;
        }

        public Wall(double width, double height) {
            if (width < 0) {
                this.width = 0;
            } else {
                this.width = width;
            }

            if (height < 0) {
                this.height = 0;
            } else {
                this.height = height;
            }
        }

        public double getWidth() {
            return width;
        }

        public double getHeight() {
            return height;
        }

        public void setWidth(double width) {
            if (width < 0) {
                this.width = 0;
            } else {
                this.width = width;
            }
        }

        public void setHeight(double height) {
            if (height < 0) {
                this.height = 0;
            } else {
                this.height = height;
            }
        }

        public double getArea() {
            return width * height;
        }
    }
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the width of the wall: ");
        double width = scanner.nextDouble();
        System.out.print("Enter the height of the wall: ");
        double height = scanner.nextDouble();
        Wall wall = new Wall(width, height);
        scanner.close();
        System.out.println("width= " + wall.getWidth());
        System.out.println("height= " + wall.getHeight());
        System.out.println("area= " + wall.getArea());
    }

}
