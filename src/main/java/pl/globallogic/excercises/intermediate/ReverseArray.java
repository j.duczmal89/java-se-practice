package pl.globallogic.excercises.intermediate;

import java.util.Random;
import java.util.Scanner;

public class ReverseArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] array = new int[size];
        fillArrayWithRandomNumbers(array);
        System.out.println("Original Array:");
        printArray(array);
        reverse(array);
        System.out.println("Reversed Array:");
        printArray(array);
        scanner.close();
    }

    public static void fillArrayWithRandomNumbers(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100); // Fills with random numbers between 0 and 99
        }
    }
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Element " + i + " contents " + array[i]);
        }
    }
    public static void reverse(int[] array) {
        int length = array.length;
        int temp;

        for (int i = 0; i < length / 2; i++) {
            temp = array[i];
            array[i] = array[length - 1 - i];
            array[length - 1 - i] = temp;
        }
    }
}
