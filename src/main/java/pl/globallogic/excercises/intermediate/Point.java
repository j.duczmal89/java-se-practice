package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class Point {
    private int x;
    private int y;

    public Point() {
        this(0, 0);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance() {
        return Math.sqrt(x * x + y * y);
    }

    public double distance(int x, int y) {
        int xDiff = this.x - x;
        int yDiff = this.y - y;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    public double distance(Point another) {
        int xDiff = this.x - another.x;
        int yDiff = this.y - another.y;
        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the x-coordinate for the first point: ");
        int x1 = scanner.nextInt();
        System.out.print("Enter the y-coordinate for the first point: ");
        int y1 = scanner.nextInt();

        System.out.print("Enter the x-coordinate for the second point: ");
        int x2 = scanner.nextInt();
        System.out.print("Enter the y-coordinate for the second point: ");
        int y2 = scanner.nextInt();

        Point first = new Point(x1, y1);
        Point second = new Point(x2, y2);

        System.out.println("Distance between the first point and (0,0): " + first.distance());
        System.out.println("Distance between the first point and the second point: " + first.distance(second));

        System.out.print("Enter the x-coordinate for a third point: ");
        int x3 = scanner.nextInt();
        System.out.print("Enter the y-coordinate for a third point: ");
        int y3 = scanner.nextInt();

        Point third = new Point(x3, y3);

        System.out.println("Distance between the first point and the third point: " + first.distance(third));

        scanner.close();
    }
}

