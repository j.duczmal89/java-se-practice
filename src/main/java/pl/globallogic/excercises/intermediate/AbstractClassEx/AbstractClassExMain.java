package pl.globallogic.excercises.intermediate.AbstractClassEx;

public class AbstractClassExMain {
    public static void main(String[] args) {
        Node root = new Node("5");
        MyLinkedList linkedList = new MyLinkedList(root);
        linkedList.addItem(new Node("3"));
        linkedList.addItem(new Node("9"));
        linkedList.addItem(new Node("8"));
        linkedList.addItem(new Node("10"));
        linkedList.traverse(linkedList.getRoot());

        SearchTree tree = new SearchTree(root);
        tree.addItem(new Node("3"));
        tree.addItem(new Node("9"));
        tree.addItem(new Node("8"));
        tree.addItem(new Node("10"));
        tree.traverse(tree.getRoot());

        tree.removeItem(new Node("3"));
        tree.traverse(tree.getRoot());
    }
}
