package pl.globallogic.excercises.intermediate;

public class NumberToWords {

        public static void main(String[] args) {
            numberToWords(123);    // One Two Three
            numberToWords(1010);   // One Zero One Zero
            numberToWords(1000);   // One Zero Zero Zero
            numberToWords(-12);    // Invalid Value
        }

        public static void numberToWords(int number) {
            if (number < 0) {
                System.out.println("Invalid Value");
                return;
            }

            int reversedNumber = reverse(number);
            int originalDigitCount = getDigitCount(number);
            int reversedDigitCount = getDigitCount(reversedNumber);

            while (reversedNumber > 0) {
                int lastDigit = reversedNumber % 10;
                reversedNumber /= 10;

                switch (lastDigit) {
                    case 0:
                        System.out.print("Zero ");
                        break;
                    case 1:
                        System.out.print("One ");
                        break;
                    case 2:
                        System.out.print("Two ");
                        break;
                    case 3:
                        System.out.print("Three ");
                        break;
                    case 4:
                        System.out.print("Four ");
                        break;
                    case 5:
                        System.out.print("Five ");
                        break;
                    case 6:
                        System.out.print("Six ");
                        break;
                    case 7:
                        System.out.print("Seven ");
                        break;
                    case 8:
                        System.out.print("Eight ");
                        break;
                    case 9:
                        System.out.print("Nine ");
                        break;
                }
            }

            // Print additional zeroes if necessary
            for (int i = reversedDigitCount; i < originalDigitCount; i++) {
                System.out.print("Zero ");
            }
        }

        public static int reverse(int number) {
            int reversed = 0;
            while (number != 0) {
                int lastDigit = number % 10;
                reversed = reversed * 10 + lastDigit;
                number /= 10;
            }
            return reversed;
        }

        public static int getDigitCount(int number) {
            if (number < 0) {
                return -1;
            }

            if (number == 0) {
                return 1;
            }

            int count = 0;
            while (number > 0) {
                count++;
                number /= 10;
            }
            return count;
        }

}
