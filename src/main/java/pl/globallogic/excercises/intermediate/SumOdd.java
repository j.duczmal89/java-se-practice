package pl.globallogic.excercises.intermediate;


import java.util.Scanner;

public class SumOdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the starting number: ");
        int firstNumber = scanner.nextInt();
        System.out.println("Enter the ending number: ");
        int secondNumber = scanner.nextInt();
        System.out.println(sumOdd(firstNumber,secondNumber));

    }
    private static boolean isOdd(int number){
        if (number<=0){
            return false;

        }
        return number % 2!= 0;
    }

    private static int sumOdd(int startingNumber, int endingNumber){
        if (startingNumber <= 0 || endingNumber <= 0 || endingNumber < startingNumber) {
            return -1;
        }

        int sum = 0;
        for (int i = startingNumber; i <= endingNumber; i++) {
            if (isOdd(i)) {
                sum += i;
            }
        }
        return sum;
    }
}

