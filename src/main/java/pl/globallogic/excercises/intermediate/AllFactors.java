package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class AllFactors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number to see its factors: ");
        int number = scanner.nextInt();
        printFactors(number);
    }

    private static void printFactors(int number){
        if (number == 1) {
            System.out.println("Invalid Value");
            return;
        }
        if (number>1){
            for (int i = 1; i<=number; i++){
                if (number % i == 0) {
                    System.out.println(i);
                }

            }
        }

    }


}
