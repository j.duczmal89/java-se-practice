package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class LastDigitChecker {
    public static void main(String[] args) {

        System.out.println(hasSameLastDigit(41, 22, 71)); // true
        System.out.println(hasSameLastDigit(23, 32, 42)); // true
        System.out.println(hasSameLastDigit(9, 99, 999)); // false

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 1st number: ");
        int number1 = scanner.nextInt();
        System.out.println("Enter 2nd number: ");
        int number2 = scanner.nextInt();
        System.out.println("Enter 3rd number: ");
        int number3 = scanner.nextInt();
        System.out.println("At least of those three numbers are ending with the same digit...." + hasSameLastDigit(number1,number2,number3));

    }

    private static boolean hasSameLastDigit(int number1, int number2, int number3){
        if (!validNumber(number1) || !validNumber(number2) || !validNumber(number3)) {
            return false;
        }

        int lastDigit1 = number1 % 10;
        int lastDigit2 = number2 % 10;
        int lastDigit3 = number3 % 10;

        return lastDigit1 == lastDigit2 || lastDigit1 == lastDigit3 || lastDigit2 == lastDigit3;
    }

    private static boolean validNumber(int number){
        return number >= 10 && number <= 1000;
    }
}
