package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class SharedDigit {
    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23)); // true
        System.out.println(hasSharedDigit(9, 99));  // false
        System.out.println(hasSharedDigit(15, 55)); // true

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter 1st number: ");
        int number1 = scanner.nextInt();
        System.out.println("Enter 2nd number: ");
        int number2 = scanner.nextInt();
        System.out.println("Checking if numbers has shared digit...  " + hasSharedDigit(number1,number2));

    }

    private static boolean hasSharedDigit(int number1, int number2){
        if (number1<10 || number1>99 || number2 <10 || number2>99){
            return false;
        }
        while (number1>10){
            int digitNumber1 = number1%10;

            while (number2>0){
                int digitNumber2 = number2%10;
                if (digitNumber1==digitNumber2){
                    return true;
                }
                number2 /=10;

            }
            number1 /= 10;
        }
        return false;
    }

}
