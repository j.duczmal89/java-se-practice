package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class FirstAndLastDigitSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int number = scanner.nextInt();
        System.out.println(sumFirstAndLastDigit(number));
    }


    public static int sumFirstAndLastDigit(int number) {
        if (number < 0) {
            return -1;
        }

        int lastDigit = number % 10;
        int firstDigit = 0;

        while (number > 0) {
            firstDigit = number % 10;
            number /= 10;
        }

        return firstDigit + lastDigit;
    }
}

