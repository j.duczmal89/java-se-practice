package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class CarpetCostCalculator {
    static class Floor {
        private double width;
        private double length;

        public Floor(double width, double length) {
            if (width < 0) {
                this.width = 0;
            } else {
                this.width = width;
            }

            if (length < 0) {
                this.length = 0;
            } else {
                this.length = length;
            }
        }

        public double getArea() {
            return width * length;
        }
    }

    static class Carpet {
        private double cost;

        public Carpet(double cost) {
            if (cost < 0) {
                this.cost = 0;
            } else {
                this.cost = cost;
            }
        }

        public double getCost() {
            return cost;
        }
    }

    static class Calculator {
        private Floor floor;
        private Carpet carpet;

        public Calculator(Floor floor, Carpet carpet) {
            this.floor = floor;
            this.carpet = carpet;
        }

        public double getTotalCost() {
            return floor.getArea() * carpet.getCost();
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the cost of carpet per square meter: ");
        double carpetCost = scanner.nextDouble();
        Carpet carpet = new Carpet(carpetCost);
        System.out.print("Enter the width of the floor: ");
        double floorWidth = scanner.nextDouble();
        System.out.print("Enter the length of the floor: ");
        double floorLength = scanner.nextDouble();
        Floor floor = new Floor(floorWidth, floorLength);
        Calculator calculator = new Calculator(floor, carpet);
        System.out.println("Total cost to cover the floor: " + calculator.getTotalCost());
        scanner.close();
    }


}
