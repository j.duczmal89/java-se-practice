package pl.globallogic.excercises.intermediate;

import java.util.Arrays;
import java.util.Scanner;

public class SortedArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] unsortedArray = getIntegers(size);
        System.out.println("Original array:");
        printArray(unsortedArray);

        int[] sortedArray = sortIntegers(unsortedArray);
        System.out.println("\nSorted array in descending order:");
        printArray(sortedArray);
    }

    public static int[] getIntegers(int size) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[size];
        System.out.println("Enter " + size + " integers:");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Element " + i + " contents " + array[i]);
        }
    }

    public static int[] sortIntegers(int[] array) {
        int[] sortedArray = Arrays.copyOf(array, array.length);
        Arrays.sort(sortedArray);
        int[] descendingArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            descendingArray[i] = sortedArray[array.length - i - 1];
        }
        return descendingArray;
    }
}
