package pl.globallogic.excercises.intermediate.PlaylistEx;

import java.util.ArrayList;
import java.util.LinkedList;

public class Playlist {
    private String name;
    private String artist;
    private SongList songs;

    public Playlist(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new SongList();
    }

    public boolean addSong(String title, double duration) {
        return songs.add(new Song(title, duration));
    }

    public Song findSong(String title) {
        return songs.findSong(title);
    }

    public Song findSong(int trackNumber) {
        return songs.findSong(trackNumber);
    }

    public boolean addToPlayList(String title, LinkedList<Song> playlist) {
        return songs.addToPlayList(title, playlist);
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
        return songs.addToPlayList(trackNumber, playlist);
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", songs=" + songs +
                '}';
    }

    private class SongList {

        private ArrayList<Song> songs;

        public SongList() {
            this.songs = new ArrayList<>();
        }

        public boolean add(Song song) {
            if (songs.contains(song)) {
                return false;
            }
            songs.add(song);
            return true;
        }

        public Song findSong(String title) {
            for (Song song : songs) {
                if (song.getTitle().equals(title)) {
                    return song;
                }
            }
            return null;
        }

        public Song findSong(int trackNumber) {
            if (trackNumber < 1 || trackNumber > songs.size()) {
                return null;
            }
            return songs.get(trackNumber - 1);
        }

        public boolean addToPlayList(String title, LinkedList<Song> playlist) {
            Song song = findSong(title);
            if (song != null) {
                playlist.add(song);
                return true;
            }
            return false;
        }

        public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
            Song song = findSong(trackNumber);
            if (song != null) {
                playlist.add(song);
                return true;
            }
            return false;
        }
    }
}
