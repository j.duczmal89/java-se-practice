package pl.globallogic.excercises.intermediate;



import java.util.Scanner;

import static pl.globallogic.excercises.basic.Ex5LeapYearCalculator.isLeapYear;

public class NumberOfDaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year: ");
        int year = scanner.nextInt();
        System.out.println("Enter the month: ");
        int month = scanner.nextInt();
        System.out.println(getDaysInMonth(year, month));

    }

    private static int getDaysInMonth(int year, int month) {
        if (month <1 || month >12 || year <1 || year >9999){
            return -1;
        }
        return switch (month) {
            case 2 -> isLeapYear(year) ? 29 : 28;
            case 4, 6, 9, 11 -> 30;
            default -> 31;
        };
    }
}

