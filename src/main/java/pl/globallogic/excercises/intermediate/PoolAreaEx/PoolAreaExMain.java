package pl.globallogic.excercises.intermediate.PoolAreaEx;

import java.util.Scanner;

public class PoolAreaExMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        /*System.out.print("Enter the width of the rectangle: ");
        double rectWidth = scanner.nextDouble();
        System.out.print("Enter the length of the rectangle: ");
        double rectLength = scanner.nextDouble();
        Rectangle rectangle = new Rectangle(rectWidth, rectLength);

        System.out.println("rectangle.width= " + rectangle.getWidth());
        System.out.println("rectangle.length= " + rectangle.getLength());
        System.out.println("rectangle.area= " + rectangle.getArea());*/


        System.out.print("Enter the width of the cuboid: ");
        double cuboidWidth = scanner.nextDouble();
        System.out.print("Enter the length of the cuboid: ");
        double cuboidLength = scanner.nextDouble();
        System.out.print("Enter the height of the cuboid: ");
        double cuboidHeight = scanner.nextDouble();
        Cuboid cuboid = new Cuboid(cuboidWidth, cuboidLength, cuboidHeight);

        System.out.println("cuboid.width= " + cuboid.getWidth());
        System.out.println("cuboid.length= " + cuboid.getLength());
        System.out.println("cuboid.area= " + cuboid.getArea());
        System.out.println("cuboid.height= " + cuboid.getHeight());
        System.out.println("cuboid.volume= " + cuboid.getVolume());

        scanner.close();
    }
}
