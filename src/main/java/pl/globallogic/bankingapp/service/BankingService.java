package pl.globallogic.bankingapp.service;

import pl.globallogic.bankingapp.model.Account;
import pl.globallogic.bankingapp.service.AccountService;
public class BankingService {
    private AccountService accountService;

    public BankingService(AccountService accountService) {
        this.accountService = accountService;
    }

    public void withdraw(int accountId,double amountToWithdraw){
        Account account = accountService.getAccountById(accountId);
        double currentBalance = account.getBalance();
        if (currentBalance < amountToWithdraw) {
            System.out.println(String.format(
                    "Amount to withdraw '%s' is greated than account '%s' balance '%s'",
                    amountToWithdraw, account.getAccountNumber(), currentBalance));
            return;
        }
        double newBalance = currentBalance - amountToWithdraw;
        account.setBalance(newBalance);
        accountService.saveAccount(account);
    }

    public void depositTo(int accountId, double amountToDeposit) {
        Account account = accountService.getAccountById(accountId);
        double currentBalance = account.getBalance();
        if (amountToDeposit > 0) {
            account.setBalance(currentBalance + amountToDeposit);
        }
        accountService.saveAccount(account);
    }

    public void transfer(int accountFromId, int accountToId, double amountToTransfer) {
        withdraw(accountFromId, amountToTransfer);
        depositTo(accountToId, amountToTransfer);
    }
}
