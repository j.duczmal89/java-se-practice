package pl.globallogic.bankingapp;

import pl.globallogic.bankingapp.model.Account;
import pl.globallogic.bankingapp.model.CreditAccount;
import pl.globallogic.bankingapp.model.SavingAccount;
public class ClassCastingSandbox {
    public static void main(String[] args) {
        Account morgageCreditAccount =
                new CreditAccount(1, "001", 20.00, 0.20,240);

        Account privateSavingAccount =
                new Account(1, "001", 20.00);
        Account mca = morgageCreditAccount;
        System.out.println(mca.getId());
    }
}
